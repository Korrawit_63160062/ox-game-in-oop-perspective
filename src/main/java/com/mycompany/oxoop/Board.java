package com.mycompany.oxoop;

import java.util.Random;

/**
 *
 * @author Acer
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer, o, x;
    private int count;
    public boolean win = false;
    public boolean draw = false;

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(100);

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = randomInt % 2 == 0 ? o : x;
        //this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean setRowCol(int row, int col) {
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            System.out.println("Your position is out of range. Please try again.");
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            System.out.println("Your position is taken. Please try again.");
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin()) {
            updateStat();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            x.draw();
            o.draw();
            this.draw = true;
            return true;
        }
        switchPlayer();
        count++;
        return true;
    }

    private void updateStat() {
        if (this.currentPlayer == o) {
            o.win();
            x.loss();
        } else {
            x.win();
            o.loss();
        }
    }

    public boolean checkWin() {
        if (table[0][0] == currentPlayer.getSymbol() && table[0][1] == currentPlayer.getSymbol() && table[0][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[1][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[1][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[2][0] == currentPlayer.getSymbol() && table[2][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][0] == currentPlayer.getSymbol() && table[1][0] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][1] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][1] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][2] == currentPlayer.getSymbol() && table[1][2] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][2] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }

        return false;

    }

    private boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

}
